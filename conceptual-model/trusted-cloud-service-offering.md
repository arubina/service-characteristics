
```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingArchitecture
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingCertificate
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingContract
TrustedCloudServiceOffering <|-- TrustedCloudServiceDataProtection
TrustedCloudServiceOffering <|-- TrustedCloudServiceInteroperability
TrustedCloudServiceOffering <|-- TrustedCloudServiceOperativeProcesses
TrustedCloudServiceOffering <|-- TrustedCloudServiceSecurity
TrustedCloudServiceOffering <|-- TrustedCloudServiceSubCompanies


```
