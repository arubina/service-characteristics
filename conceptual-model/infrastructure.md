# Infrastructure

```mermaid
classDiagram

ServiceOffering <| -- Infrastructure
Infrastructure <|-- Compute
Infrastructure <|-- Storage
Infrastructure <|-- Network
```
