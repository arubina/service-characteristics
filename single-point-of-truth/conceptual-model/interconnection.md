```mermaid
classDiagram

class Interconnection {
    <<abstract>>
}

PhysicalResource <|-- PhysicalInterconnection

Interconnection <|-- PhysicalInterconnection
Interconnection <|-- VirtualInterconnection

InstantiatedVirtualResource <|-- VirtualInterconnection
```